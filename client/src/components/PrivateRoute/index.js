import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useKeycloak } from "@react-keycloak/web";

const PrivateRoute = ({ children, ...rest }) => {
    const { keycloak } = useKeycloak()
    return <Route {...rest} render={() => (keycloak.authenticated ? children : <Redirect to='/login' />)} />;
};

export default PrivateRoute;
