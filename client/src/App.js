import GlobalStyle from "./GlobalStyle";
import Login from "./pages/Login";
import PrivateRoute from "./components/PrivateRoute";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import NotFound from "./pages/NotFound";
import { useKeycloak } from "@react-keycloak/web";
import { } from "./verifyToken"
import Test from "./pages/Test";


function App() {
    const { keycloak } = useKeycloak()
    console.log(keycloak.token);
    console.log(keycloak.tokenParsed);
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/'>
                    <Redirect to='/login' />
                </Route>
                <PrivateRoute path="/test">
                    <Test />
                </PrivateRoute>
                <Route path='/login' component={Login} />
                <PrivateRoute path='/dashboard'>
                    <Dashboard />
                </PrivateRoute>
                <Route path='/*' component={NotFound} />
            </Switch>
            <GlobalStyle />
        </BrowserRouter>
    );
}

export default App;
