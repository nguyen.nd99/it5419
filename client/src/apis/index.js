import * as auth from "./auth";
import * as store from "./store";
import * as user from "./user";
import * as role from "./role";
import * as product from "./product";
import * as category from "./category";
import * as item from "./item";
import * as order from "./order";

const allApi = {
    auth,
    store,
    user,
    role,
    product,
    category,
    item,
    order,
};

export default allApi;
